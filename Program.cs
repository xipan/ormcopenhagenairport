using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopenhagenAirport
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            Console.WriteLine("Hello World!");
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 

            Console.WriteLine("Departure Airport: CPH Copenhagen Kastrup Airport(CPH)");
            Console.WriteLine("Input Destination Airport:");
            string destinationAirport = Console.ReadLine();

            CopenhagenAirportEntities dbcontext = new CopenhagenAirportEntities();
            var flightLine = (from flightline in dbcontext.flightline
                             join depairport in  dbcontext.airport on flightline.departureAirportId equals depairport.id
                             join desairport in dbcontext.airport on flightline.destinationAirportId equals desairport.id
                             join airline in dbcontext.airline on flightline.airlineId equals airline.id
                             select new
                             {
                                 departureAirportName = depairport.name,
                                 departureAirportCode = depairport.iataCode,
                                 destinationAirportName = desairport.name,                                 
                                 destinationAirporCode = desairport.iataCode,
                                 airlineName = airline.name,
                                 airlineCode = airline.iataCode,
                                 flightTime = flightline.flightTime,
                                 flightlineCode = flightline.flightlineCode
                             }).ToList();

            var test = flightLine.ToList();

            var searchFlightLine = (from s in flightLine
                                   where s.destinationAirportName.ToLower().Contains(destinationAirport.ToString())
                                   select s).ToList();

            foreach (var item in searchFlightLine)
            {                
                Console.WriteLine(item.departureAirportName.ToString() + "----" + item.destinationAirportName.ToString() + "----" + item.airlineName.ToString());
            }

            Console.Read();

        }
    }
}
